--[[
  nill..
    number 1 1.23 0.1 44
    string 'Hello' "Hello world"
    boolean true false
    table
--]]

local a = 2 
-- 2 + 5 = a
-- 1 + a = 10
-- a = 1
print(a)
print(10 + a)
-- a + 5 = 7
